## Exemple myweb21

Fitxers:
  * Dockerfile
  * index.html
  * startup.sh

#### Generar imatge:
```
docker build -t marleneflor/myweb21:v2 .
```

#### Executar interactivament:
```
docker run --rm --name web -h web -it marleneflor/myweb21:v2 /bin/bash
```

#### Executar en detach:
```
docker run --rm --name web -h web -d marleneflor/myweb21:v2
```

