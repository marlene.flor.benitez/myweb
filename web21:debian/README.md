# web21:debian
# @MarleneFlor ASIX-m05

## Web21:debian -- Servidor apache detatch
```
docker run --rm -d marleneflor/web21:debian
```
## Web21:debian -- Servidor apache interactiu
```
docker run --rm -it marleneflor/web21:debian /bin/bash
```
## Web21:debian -- Crear imatge
```
docker build -t marleneflor/web21:debian .
```
## Entrar al container engegat en detach
```
docker exec -it nom_container /bin/bash
```
## Assignar un port dinàmic amb al container
```
docker run --rm -P -d marleneflor/web21:debian
```
