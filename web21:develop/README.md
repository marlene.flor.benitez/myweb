## Propagar un port específic
```
docker run --rm --name web -h web -p 8080:80 -d marleneflor/web21:develop
```
## Muntar un fitxer al localhost perque el container ho utilitzi
```
docker run --rm --name web -h web -p 8080:80 --mount type=bind,src=/tmp/index.html,dst=/var/www/html/index.html -d marleneflor/web21:develop
```
## Muntar un dir del anfitrio al container
```
docker run --rm --name web -h web -p 8080:80 --mount type=bind,src=/tmp/web,dst=/var/www/html/ -d marleneflor/web21:develop
```
## Lligar volums a containers
```
docker run --rm --name web -h web -p 8080:80 -v web1:/var/www/html -d marleneflor/web21:develop
```
## Mostra quins containers estan en una maquina
# desplegament d'aplicacions al cloud
```
docker run --rm -p 9000:8080 -v /var/run/docker.sock:/var/run/docker.sock -d dockersamples/visualizer:stable
```
## Dashboard de gestio de containers a la meva màquina
# detalls sobre els containers a la meva màquina
```
docker run --rm -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -d portainer/portainer
```
## Pasar-li variables a un container
> env per veure les variables d'entorn
```
docker run -e nom="pere" --rm -it debian env
```
# 
